// 斐波那契数列
function feibo(n){
    var fei=[0,1];
    for(var i=2;i<=n;i++){
        fei[i]=fei[i-1]+fei[i-2];
    }
    return fei;
}
console.log(feibo(13));


//用递归写二分查找
function binarySearch(arr,low,high,key){
    if(low>high){
        return -1;
    }
    var mid=Math.floor((low+high)/2);
    if(arr[mid]==key){
        return mid;
    }else if(arr[mid]>key){
        return binarySearch(arr,low,mid-1,key);
    }else{
        return binarySearch(arr,mid+1,high,key);
    }
}
a